# tg-bomb v0.2.0

## a program used to bulk send messages to a person in telegram in rust


> To use:
```bash
git clone https://gitlab.com/cyberknight777/tg-bomb
cd tg-bomb
cargo run --release
```

# License

## This program is released under MIT License
